CREATE TABLE Users (
	id serial,
	email TEXT UNIQUE,
	password TEXT NOT NULL,
	firstname TEXT NOT NULL,
	secondname TEXT NOT NULL,
	city integer NOT NULL,
	about TEXT NOT NULL,
	birthday DATETIME NOT NULL,
	datacreate DATETIME NOT NULL,
	vk_url TEXT NOT NULL,
	twitter_url TEXT NOT NULL,
	phone TEXT NOT NULL,
	sex TEXT NOT NULL,
	type integer NOT NULL DEFAULT '0',
	popularity FLOAT NOT NULL,
	activities FLOAT NOT NULL,
	CONSTRAINT Users_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE UserType (
	id serial NOT NULL,
	type TEXT NOT NULL,
	CONSTRAINT UserType_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Friendinvites (
	id serial NOT NULL,
	from integer NOT NULL,
	to integer NOT NULL,
	CONSTRAINT Friendinvites_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Friends (
	id serial NOT NULL,
	user_id integer NOT NULL,
	friend_id integer NOT NULL,
	CONSTRAINT Friends_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Chat (
	id serial NOT NULL,
	name TEXT NOT NULL,
	created_at DATETIME NOT NULL,
	created_by integer NOT NULL,
	level_chat integer NOT NULL,
	numinchat integer NOT NULL,
	CONSTRAINT Chat_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Invites (
	id serial NOT NULL,
	chat_id integer NOT NULL,
	created_at DATETIME NOT NULL,
	from integer NOT NULL,
	to integer NOT NULL,
	CONSTRAINT Invites_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Message (
	id serial NOT NULL,
	autor_id integer NOT NULL,
	chat_id integer NOT NULL,
	created_at DATETIME NOT NULL,
	text TEXT NOT NULL,
	CONSTRAINT Message_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE UserInChat (
	id serial NOT NULL,
	chat_id integer NOT NULL,
	created_by integer NOT NULL,
	created_at DATETIME NOT NULL,
	user_level integer NOT NULL,
	CONSTRAINT UserInChat_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Conference (
	id serial NOT NULL,
	city integer NOT NULL,
	datetime DATETIME NOT NULL,
	chat integer NOT NULL,
	CONSTRAINT Conference_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Report (
	id serial NOT NULL,
	url TEXT NOT NULL,
	autor integer NOT NULL,
	datetime DATETIME NOT NULL,
	conference integer NOT NULL,
	CONSTRAINT Report_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE City (
	id serial NOT NULL,
	name TEXT NOT NULL UNIQUE,
	country integer NOT NULL,
	CONSTRAINT City_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);



CREATE TABLE Country (
	id serial NOT NULL,
	name TEXT NOT NULL UNIQUE,
	CONSTRAINT Country_pk PRIMARY KEY (id)
) WITH (
  OIDS=FALSE
);




ALTER TABLE Users ADD CONSTRAINT Users_fk0 FOREIGN KEY (city) REFERENCES City(id);
ALTER TABLE Users ADD CONSTRAINT Users_fk1 FOREIGN KEY (type) REFERENCES UserType(id);
ALTER TABLE Users ADD CONSTRAINT Users_fk1 FOREIGN KEY (type) REFERENCES UserType(id);

ALTER TABLE Friendinvites ADD CONSTRAINT Friendinvites_fk0 FOREIGN KEY (from) REFERENCES Users(id);
ALTER TABLE Friendinvites ADD CONSTRAINT Friendinvites_fk1 FOREIGN KEY (to) REFERENCES Users(id);

ALTER TABLE Friends ADD CONSTRAINT Friends_fk0 FOREIGN KEY (user_id) REFERENCES Users(id);
ALTER TABLE Friends ADD CONSTRAINT Friends_fk1 FOREIGN KEY (friend_id) REFERENCES Users(id);

ALTER TABLE Chat ADD CONSTRAINT Chat_fk0 FOREIGN KEY (created_by) REFERENCES Users(id);

ALTER TABLE Invites ADD CONSTRAINT Invites_fk0 FOREIGN KEY (chat_id) REFERENCES Chat(id);
ALTER TABLE Invites ADD CONSTRAINT Invites_fk1 FOREIGN KEY (from) REFERENCES Users(id);
ALTER TABLE Invites ADD CONSTRAINT Invites_fk2 FOREIGN KEY (to) REFERENCES Users(id);

ALTER TABLE Message ADD CONSTRAINT Message_fk0 FOREIGN KEY (autor_id) REFERENCES UserInChat(id);
ALTER TABLE Message ADD CONSTRAINT Message_fk1 FOREIGN KEY (chat_id) REFERENCES Chat(id);

ALTER TABLE UserInChat ADD CONSTRAINT UserInChat_fk0 FOREIGN KEY (chat_id) REFERENCES Chat(id);
ALTER TABLE UserInChat ADD CONSTRAINT UserInChat_fk1 FOREIGN KEY (created_by) REFERENCES Users(id);

ALTER TABLE Conference ADD CONSTRAINT Conference_fk0 FOREIGN KEY (city) REFERENCES City(id);
ALTER TABLE Conference ADD CONSTRAINT Conference_fk1 FOREIGN KEY (chat) REFERENCES Chat(id);

ALTER TABLE Report ADD CONSTRAINT Report_fk0 FOREIGN KEY (autor) REFERENCES Users(id);
ALTER TABLE Report ADD CONSTRAINT Report_fk1 FOREIGN KEY (conference) REFERENCES Conference(id);

ALTER TABLE City ADD CONSTRAINT City_fk0 FOREIGN KEY (country) REFERENCES Country(id);
ALTER TABLE City ADD CONSTRAINT City_fk0 FOREIGN KEY (country) REFERENCES Country(id);
