ДЗ 11. Гл. 12: 


# Глава 12

```
CREATE TABLE books( 
	book_id integer PRIMARY KEY,
	book_description text);
				
COPY books FROM '/books3.txt';

ALTER TABLE books ADD COLUMN ts_description tsvector;

UPDATE books SET ts_description= to_tsvector( 'russian', book_description);

CREATE INDEX books_idx ON books USING GIN(ts_description);

SELECT book_id, book_description FROM books
WHERE ts_description @@ (to_tsquery( 'Моргунов' ) || to_tsquery( 'Дима' ));
```

![11_12_1.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%2311/img/11_12_1.png)

![11_12_2.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%2311/img/11_12_2.png)

![11_12_3.png](https://gitlab.com/CodingSquire/postgreesql/raw/master/homework%20%2311/img/11_12_3.png)